import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd'
    },
    {
      id: 'UC-02',
      name: 'Kamer reserveren',
      description: 'Hiermee kan een gast een kamer boeken.',
      scenario: ['Selecteer kamer', 'Vul adres- en betaalgegevens in', 'Boek kamer'],
      actor: 'Gast',
      precondition: 'Adres en betaalgegevens ingevoerd.',
      postcondition: 'De gast heeft de kamer geboekt.'
    },
    {
      id: 'UC-03',
      name: 'Faciliteit toevoegen',
      description: 'Hiermee kan een gast bij het boeken van een kamer faciliteiten toevoegen aan de boeking.',
      scenario: ['Selecteer faciliteit', 'Voeg faciliteit toe aan reservering'],
      actor: 'Gast',
      precondition: 'Gast heeft een kamer geselecteerd.',
      postcondition: 'De faciliteiten zijn toegevoegd aan de boeking.'
    }
  ]

  authorID = '2147965'
  authorName = 'Ties Mulder'
  authorImg = '"assets/images/person.png"'

  projectName = 'FijneDagen'

  constructor() {}

  ngOnInit() {}
}
